from django.contrib import admin
from .models import Emails as EmailModel
from .forms import EmailsForm
# Register your models here.

class EmailsAdmin(admin.ModelAdmin):
    list_display = ["user",
                    "email_sender",
                    "email_receiver",
                    "subject"]
    form = EmailsForm
    # class Meta:
    #     model = EmailModel


admin.site.register(EmailModel,EmailsAdmin)
