from rest_framework.views import APIView
from rest_framework import generics, mixins
from rest_framework.response import Response

from emails.models import Emails
from .serializers import EmailSerializer

# class EmailsListSearchAPIView(APIView):
#     permission_classes = []
#     authentication = []
#
#     def get(self, request, format=None):
#         qs = Emails.objects.all()
#         serializer = EmailSerializer(qs, many=True)
#         return Response(serializer.data)


class EmailsAPIView(mixins.CreateModelMixin,
                    generics.ListAPIView):
    permission_classes = []
    authentication = []
    serializer_class = EmailSerializer

    def get_queryset(self):
        qs = Emails.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(content__icontains=query)
        return qs

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

# class EmailsCreateAPIView(generics.CreateAPIView):
#     permission_classes = []
#     authentication = []
#     queryset = Emails.objects.all()
#     serializer_class = EmailSerializer

class EmailsDetailsAPIView(mixins.DestroyModelMixin,
                           mixins.UpdateModelMixin,
                           generics.RetrieveAPIView):
    permission_classes = []
    authentication = []
    queryset = Emails.objects.all()
    serializer_class = EmailSerializer
    # lookup_field = "email_receiver"
    lookup_field = "id"

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
