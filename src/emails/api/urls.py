from django.conf.urls import url
from .views import (EmailsAPIView,
                    # EmailsCreateAPIView,
                    EmailsDetailsAPIView)

urlpatterns = [
    url(r'^$', EmailsAPIView.as_view()),
    # url(r'^create/$', EmailsCreateAPIView.as_view()),
    url(r'^(?P<id>.*)/$', EmailsDetailsAPIView.as_view()),
]
