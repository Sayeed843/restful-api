from rest_framework import serializers
from emails.models import Emails


class EmailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Emails
        fields = [
            "user",
            "email_sender",
            "email_receiver",
            "subject",
            "body"
        ]

    # def validate_subject(self,data):
    #     subject = data.get("subject", None)
    #     if len(subject) > 200:
    #         raise serializers.ValidateError("Subject is too much long")
    #     return subject

    def validate(self, data):
        email_sender = data.get("email_sender", None)
        email_receiver = data.get("email_receiver", None)
        subject = data.get("subject", None)
        body = data.get("body", None)

        if subject == "" and body == "":
            subject = None
            body = None

        if subject is None and body is None:
            raise serializers.ValidationError("Subject or Body is required")

        if email_sender is None or email_receiver is None:
            raise serializers.ValidationError("Email sender and receiver must be valid")
        return data
