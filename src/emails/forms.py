from django import forms
from .models import Emails
import re   # regular expression



class EmailsForm(forms.ModelForm):
    class Meta:
        model = Emails
        fields = [
            "user",
            "email_sender",
            "email_receiver",
            "subject",
            "body"
        ]

    # def clean_email_sender(self, *args, **kwargs):
    #     email_sender = self.cleaned_data.get("email_sender")


    def clean_subject(self, *args, **kwargs):
        subject = self.cleaned_data.get("subject")
        print("Subject Length:"+str(len(subject)))

        if len(subject) > 150:
            raise forms.ValidationError("Subject is too long")
        return subject

    def clean(self,*args, **kwargs):
        data = self.cleaned_data
        email_sender = data.get('email_sender',None)
        email_receiver = data.get('email_receiver',None)
        subject = data.get("subject",None)
        body = data.get("body",None)

        if subject == "" and body == "":
            subject = None
            body = None

        if subject is None and body is None:
            raise forms.ValidationError("Subject or Body is required")


        if email_sender is None or email_receiver is None:
            raise forms.ValidationError("Email sender and receiver must be valid")

        return super().clean(*args, **kwargs)
