from django.db import models
from django.conf import settings
import json



class Emails(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    email_sender = models.EmailField()
    email_receiver = models.EmailField()
    subject = models.TextField(max_length=1500, null=True, blank=True)
    body = models.TextField(null=True, blank=True)
    times = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.subject
